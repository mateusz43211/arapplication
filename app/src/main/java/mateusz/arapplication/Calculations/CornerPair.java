package mateusz.arapplication.Calculations;

import com.google.ar.sceneform.math.Vector3;

public class CornerPair {
    private Corner c1;
    private Corner c2;

    public CornerPair(Corner corner1, Corner corner2) {
        this.c1 = corner1;
        this.c2 = corner2;
    }

    public Vector3 getCenterPoint() {
        return Vector3.add(c1.getPosition(), c2.getPosition()).scaled(0.5f);
    }

    public Vector3 getDifference() {
        return Vector3.subtract(c1.getPosition(), c2.getPosition());
    }

    @Override
    public int hashCode() {
        return c1.hashCode() + c2.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof CornerPair))
            return false;
        CornerPair cp = (CornerPair) (obj);
        return (cp.c1.equals(c1) && cp.c2.equals(c2));
    }
}
