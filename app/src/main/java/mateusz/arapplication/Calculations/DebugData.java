package mateusz.arapplication.Calculations;

import com.google.ar.sceneform.math.Vector3;

import java.util.ArrayList;

public class DebugData {
    private ArrayList<float[]> corners = new ArrayList<>();

    public DebugData() {
        float array[] = new float[]{-0.24f, -0.2f, 0.43f};
        corners.add(array);
        array = new float[]{0.412f, -0.22f, 0.39f};
        corners.add(array);
        array = new float[]{0.43f, -0.2f, -0.2f};
        corners.add(array);
        array = new float[]{-0.27f, -0.17702912f, -0.2f};
        corners.add(array);
    }

    public ArrayList<Vector3> getCornersVectors() {
        ArrayList<Vector3> newCorners = new ArrayList<>();
        Vector3 tmp;
        for (float[] c : corners) {
            tmp = new Vector3(c[0], c[1], c[2]);
            newCorners.add(tmp);
        }
        return newCorners;
    }

    public ArrayList<float[]> getCornersArrays() {
        return corners;
    }
}
