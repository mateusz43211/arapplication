package mateusz.arapplication.Calculations;

import com.google.ar.sceneform.math.Vector3;

public class Line {
    private Vector3 point1;
    private Vector3 point2;

    public Line(Vector3 firstPoint, Vector3 secondPoint) {
        point1 = firstPoint;
        point2 = secondPoint;
    }

    public Vector3 getCenterPoint() {
        return Vector3.add(point1, point2).scaled(0.5f);
    }

    public Vector3 getDifference() {
        return Vector3.subtract(point1, point2);
    }
}
