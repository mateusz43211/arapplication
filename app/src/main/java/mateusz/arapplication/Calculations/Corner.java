package mateusz.arapplication.Calculations;

import android.util.Log;

import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Plane;
import com.google.ar.core.Trackable;
import com.google.ar.sceneform.math.Vector3;

import java.util.ArrayList;

import mateusz.arapplication.AR.ARUtils;

public class Corner {
    private final String TAG = "Corner.class";
    private static final int TABLE_SIZE = 3;
    private SurfaceData[] planes;
    private MatrixEquation matrixEquation;
    private Vector3 cornerPosition = Vector3.zero();

    public Corner(TrackablePair pair, Trackable floor) {
        ArrayList<Trackable> trackables = pair.getTrackables();
        trackables.add(floor);
        if (hasProperlySize(trackables)) {
            createPlanesData(trackables);
            matrixEquation = new MatrixEquation(planes);
        }
    }

    private boolean hasProperlySize(ArrayList<Trackable> trackables) {
        if (trackables.size() == TABLE_SIZE)
            return true;
        else
            return false;
    }

    private void createPlanesData(ArrayList<Trackable> trackables) {
        planes = new SurfaceData[TABLE_SIZE];
        for (int i = 0; i < trackables.size(); i++) {
            Trackable t = trackables.get(i);

            if (t instanceof AugmentedImage) {
                planes[i] = new SurfaceDataFromImage((AugmentedImage) t);
            } else if (t instanceof Plane) {
                planes[i] = new SurfaceDataFromPlane((Plane) t);
            }
        }
    }

    public void update() {
        updatePlanesData();
        updateMatrixData();
        updateCornerCoordinates();
        ARUtils.logCorner(cornerPosition, TAG);
    }

    private void updatePlanesData() {
        for (SurfaceData pd : planes) {
            pd.updateData();
        }
    }

    private void updateMatrixData() {
        matrixEquation.updateData(planes);
    }

    private void updateCornerCoordinates() {
        try {
            float[] newData = MatrixEquationSolver.solveEquation(matrixEquation);
            cornerPosition.set(newData[0], newData[1], newData[2]);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Illegal matirx argument in equation Solver");
        }
    }

    public Vector3 getPosition() {
        return cornerPosition;
    }

}
