package mateusz.arapplication.Calculations;

import com.google.ar.core.Plane;

public class SurfaceDataFromPlane extends SurfaceData {
    private final Plane plane;

    public SurfaceDataFromPlane(Plane plane) {
        this.plane = plane;
        updateData();
    }

    @Override
    public void updateData() {
        this.normal = plane.getCenterPose().getYAxis();
        this.translation = plane.getCenterPose().getTranslation();
    }
}
