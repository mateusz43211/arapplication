package mateusz.arapplication.Calculations;

import com.google.ar.core.Trackable;

import mateusz.arapplication.AR.ImageNode;

public interface OnImageTouchedListener {
    void checkTouchedImageNode(ImageNode imageNode);
}
