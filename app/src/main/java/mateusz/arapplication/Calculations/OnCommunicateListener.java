package mateusz.arapplication.Calculations;

public interface OnCommunicateListener {
    void showConfirmToFloorTrackable();

    void showFloorIsNotFound();
}
