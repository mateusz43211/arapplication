package mateusz.arapplication.Calculations;

import org.ejml.data.SingularMatrixException;
import org.ejml.simple.SimpleMatrix;

public class MatrixEquationSolver {
    public static final int MATRIX_SIZE = 3;

    public static float[] solveEquation(MatrixEquation matrixEquation) {

        SimpleMatrix rotation = new SimpleMatrix(matrixEquation.getA());
        SimpleMatrix translation = createColumnMatrix(matrixEquation.getB());

        try {
            SimpleMatrix point = rotation.solve(translation);
            float[] result = getResultArray(point);
            return result;
        } catch (SingularMatrixException e) {
            throw new IllegalArgumentException();
        }
    }

    private static float[] getResultArray(SimpleMatrix point) {
        float result[] = new float[3];
        result[0] = (float) point.get(0, 0);
        result[1] = (float) point.get(1, 0);
        result[2] = (float) point.get(2, 0);
        return result;
    }

    private static SimpleMatrix createColumnMatrix(float[] inputData) {
        SimpleMatrix matrix = new SimpleMatrix(MATRIX_SIZE, 1);
        for (int i = 0; i < MATRIX_SIZE; i++) {
            matrix.set(i, inputData[i]);
        }
        return matrix;
    }

}
