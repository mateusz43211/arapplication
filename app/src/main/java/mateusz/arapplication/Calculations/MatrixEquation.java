package mateusz.arapplication.Calculations;

public class MatrixEquation {
    private final int MATRIX_SIZE = 3;
    private float[][] A;
    private float[] b;

    public MatrixEquation(SurfaceData[] planes) {
        A = new float[MATRIX_SIZE][MATRIX_SIZE];
        b = new float[MATRIX_SIZE];
        updateData(planes);
    }

    public float[][] getA() {
        return A;
    }

    public float[] getB() {
        return b;
    }

    public void updateData(SurfaceData[] planes) {
        if (planes.length == MATRIX_SIZE) {
            fillArrays(planes);
        }
    }

    private void fillArrays(SurfaceData[] planes) {
        for (int i = 0; i < planes.length; ++i) {
            this.b[i] = planes[i].getPlaneOffset();

            float[] normal = planes[i].getNormal();
            for (int j = 0; j < MATRIX_SIZE; j++) {
                this.A[i][j] = normal[j];
            }
        }
    }


}
