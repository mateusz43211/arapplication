package mateusz.arapplication.Calculations;

import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Plane;
import com.google.ar.core.Trackable;

public abstract class SurfaceData {
    private final String TAG = "SurfaceData.class";
    protected float[] normal;
    protected float[] translation;

    public SurfaceData() {

    }

    public abstract void updateData();

    public float[] getNormal() {
        return normal;
    }

    public float getPlaneOffset() {
        return normal[0] * translation[0] + normal[1] * translation[1] + normal[2] * translation[2];
    }
}
