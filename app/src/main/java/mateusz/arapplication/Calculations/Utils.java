package mateusz.arapplication.Calculations;

import com.google.ar.sceneform.math.Vector3;

public class Utils {
    public static Vector3 getVectorFromArray(float[] coordinates) {
        return new Vector3(coordinates[0], coordinates[1], coordinates[2]);
    }

}
