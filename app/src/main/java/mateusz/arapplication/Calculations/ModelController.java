package mateusz.arapplication.Calculations;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;

import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Trackable;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mateusz.arapplication.AR.ARUtils;
import mateusz.arapplication.AR.AugmentedImageNode;
import mateusz.arapplication.AR.ImageNode;

public class ModelController {
    private final String TAG = "ModelController.class";
    private final float MINIMAL_DOT_PRODUCT_FOR_UPWARD_FACING = 0.95f;
    private final Vector3 UP_DIRECTION = Vector3.up();
    private final Color IMAGE_MARKER_COLOR = new Color(android.graphics.Color.YELLOW);
    private final float MARKER_SIZE = 0.05f;

    private ModelHandler modelHandler;
    private final OnCommunicateListener communicateListener;
    private Scene scene;
    private final Context appContext;

    private Trackable currentFloorCandidate;
    private boolean floorFound = false;
    private ImageNode lastTouchedImageNode = null;

    private final Map<AugmentedImage, AugmentedImageNode> augmentedImageNodeMap = new HashMap<>();
    private final Map<AugmentedImage, ImageNode> imageNodeMap = new HashMap<>();

    public ModelController(Context context, Scene scene, OnCommunicateListener listener) {
        this.appContext = context;
        this.scene = scene;
        this.communicateListener = listener;
        modelHandler = new ModelHandler(context, scene, communicateListener);
    }

    public void addCurrentFloorTrackable() {
        modelHandler.setFloorTrackable(currentFloorCandidate);
        floorFound = true;
    }

    public void updateArScene(Frame frame) {
        if (frame == null) {
            return;
        }
        if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
            return;
        }

        modelHandler.updateModel();

        for (Map.Entry<AugmentedImage, ImageNode> imageNode : imageNodeMap.entrySet()) {
            imageNode.getValue().update();
        }
        // visualization for detected images
        Collection<AugmentedImage> updatedAugmentedImages = frame.getUpdatedTrackables(AugmentedImage.class);
        for (AugmentedImage image : updatedAugmentedImages) {
            ARUtils.logAugmentedImage(image, "AID");
            switch (image.getTrackingState()) {
                case PAUSED:
                    break;

                case TRACKING:
                    if (!augmentedImageNodeMap.containsKey(image)) {
                        AugmentedImageNode node = new AugmentedImageNode(appContext);
                        node.setImage(image);
                        augmentedImageNodeMap.put(image, node);
                        scene.addChild(node);

                        createNodeForImage(image);
                    }
                    break;

                case STOPPED:
                    augmentedImageNodeMap.remove(image);
                    break;
            }
        }
    }

    public void findTrackablesFromTap(Frame frame, MotionEvent tap) {
        if (tap != null && frame.getCamera().getTrackingState() == TrackingState.TRACKING) {
            for (HitResult hit : frame.hitTest(tap)) {
                Trackable trackable = hit.getTrackable();
                if (trackable instanceof AugmentedImage) {
                    checkTouchEventForAugmentedImage(trackable);
                    return;
                } else if (!floorFound) {
                    if (trackable instanceof Plane && ((Plane) trackable).isPoseInPolygon(hit.getHitPose())) {
                        checkTouchEventForPlane(trackable);
                    }
                }
            }
        }
    }

    private void checkTouchEventForAugmentedImage(Trackable trackable) {
        Log.d("HitAction", "Image was touched ");
        AugmentedImage image = (AugmentedImage) trackable;

        if (!floorFound && isAugmentedImageUpwardFacing(image)) {
            currentFloorCandidate = trackable;
            communicateListener.showConfirmToFloorTrackable();
            return;
        }

        checkTouchedImageNode(imageNodeMap.get(image));
    }

    private boolean isAugmentedImageUpwardFacing(AugmentedImage image) {
        Vector3 imageDirection = Utils.getVectorFromArray(image.getCenterPose().getYAxis()).normalized();

        float dotProduct = Vector3.dot(imageDirection, UP_DIRECTION);
        Log.d(TAG, "DotProductForImage: " + dotProduct);
        if (dotProduct > MINIMAL_DOT_PRODUCT_FOR_UPWARD_FACING) {
            return true;
        } else {
            return false;
        }
    }

    private void checkTouchedImageNode(ImageNode imageNode) {
        if (lastTouchedImageNode == imageNode) {
            lastTouchedImageNode.setAsUntouched();
            lastTouchedImageNode = null;
        } else {
            if (lastTouchedImageNode == null) {
                lastTouchedImageNode = imageNode;
                lastTouchedImageNode.setAsTouched();
            } else {
                modelHandler.checkCornerForTrackables(lastTouchedImageNode.getImage(), imageNode.getImage());
                lastTouchedImageNode.setAsUntouched();
                lastTouchedImageNode = imageNode;
                lastTouchedImageNode.setAsTouched();
            }
        }
    }


    private void checkTouchEventForPlane(Trackable trackable) {
        Log.d("HitAction", "Plane was touched ");
        Plane plane = (Plane) trackable;

        if (plane.getType() == Plane.Type.HORIZONTAL_UPWARD_FACING) {
            currentFloorCandidate = trackable;
            communicateListener.showConfirmToFloorTrackable();
        }
    }

    public void saveModel() {
        modelHandler.saveModel();
    }

    private void createNodeForImage(AugmentedImage image) {
        MaterialFactory.makeOpaqueWithColor(appContext, IMAGE_MARKER_COLOR)
                .thenAccept(material -> {
                    ModelRenderable model = ShapeFactory
                            .makeSphere(MARKER_SIZE, Vector3.zero(), material);
                    ImageNode imageNode = new ImageNode(image, model);
                    imageNode.setParent(scene);
                    imageNodeMap.put(image, imageNode);
                });
    }


}
