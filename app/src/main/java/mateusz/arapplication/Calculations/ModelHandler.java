package mateusz.arapplication.Calculations;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Trackable;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mateusz.arapplication.AR.CornerNode;
import mateusz.arapplication.AR.ImageNode;
import mateusz.arapplication.AR.LineNode;
import mateusz.arapplication.Measurement;

public class ModelHandler implements OnCornerTouchedListener {
    private final String TAG = "ModelHandler.class";
    private final float CORNER_RADIUS = 0.05f;
    private Color cornerColor = new Color(android.graphics.Color.RED);
    private Color lineColor = new Color(android.graphics.Color.CYAN);

    private Trackable floorTrackable;
    private final Map<TrackablePair, Corner> cornerMap = new HashMap<>();
    private final Map<Corner, CornerNode> cornerNodeMap = new HashMap<>();
    private final Map<CornerPair, LineNode> lineNodeMap = new HashMap<>();

    private Scene scene;
    private Context appContext;
    private OnCommunicateListener communicateListener;
    private CornerNode lastTouchedCornerNode = null;


    public ModelHandler(Context context, Scene scene, OnCommunicateListener listener) {
        this.appContext = context;
        this.scene = scene;
        this.communicateListener = listener;
    }

    public void checkCornerForTrackables(Trackable t1, Trackable t2) {
        if (!isCornerForTrackables(t1, t2) && !isCornerForTrackables(t2, t1) && floorTrackable != null) {
            addCornerForTrackables(t1, t2);
        } else if (floorTrackable == null) {
            // TODO throw there is no floor
            communicateListener.showFloorIsNotFound();
            return;
        }
    }

    private boolean isCornerForTrackables(Trackable t1, Trackable t2) {
        TrackablePair pair = new TrackablePair(t1, t2);
        if (cornerMap.containsKey(pair))
            return true;
        else
            return false;
    }

    private void addCornerForTrackables(Trackable t1, Trackable t2) {
        Log.d(TAG, "Adding Corner For Trackables");
        TrackablePair pair = new TrackablePair(t1, t2);
        Corner corner = new Corner(pair, floorTrackable);
        cornerMap.put(pair, corner);
        createNodeForCorner(corner);
    }

    private void createNodeForCorner(Corner corner) {
        MaterialFactory.makeOpaqueWithColor(appContext, cornerColor)
                .thenAccept(material -> {
                    ModelRenderable model = ShapeFactory
                            .makeSphere(CORNER_RADIUS, Vector3.zero(), material);
                    CornerNode node = new CornerNode(this, corner, model);
                    node.setParent(scene);
                    cornerNodeMap.put(corner, node);
                });
    }

    @Override
    public void checkTouchedCornerNode(CornerNode cornerNode) {
        if (lastTouchedCornerNode == cornerNode) {
            lastTouchedCornerNode.setAsUntouched();
            lastTouchedCornerNode = null;
        } else {
            if (lastTouchedCornerNode == null) {
                cornerNode.setAsTouched();
                lastTouchedCornerNode = cornerNode;
            } else {
                checkLineForCorners(lastTouchedCornerNode.getCorner(), cornerNode.getCorner());
                lastTouchedCornerNode.setAsUntouched();
                cornerNode.setAsTouched();
                lastTouchedCornerNode = cornerNode;
            }
        }
    }

    public void checkLineForCorners(Corner c1, Corner c2) {
        if (!isLineForCorners(c1, c2) && !isLineForCorners(c2, c1)) {
            Log.d(TAG, "Adding Line for Corners");
            addLineForCorners(c1, c2);
        }
    }

    private boolean isLineForCorners(Corner c1, Corner c2) {
        Log.d(TAG, "LineNodeMap size: " + lineNodeMap.size());
        CornerPair cornerPair = new CornerPair(c1, c2);
        if (lineNodeMap.containsKey(cornerPair)) {
            return true;
        } else {
            return false;
        }
    }

    private void addLineForCorners(Corner c1, Corner c2) {
        CornerPair cornerPair = new CornerPair(c1, c2);
        final Vector3 difference = cornerPair.getDifference();
        MaterialFactory.makeOpaqueWithColor(appContext, lineColor)
                .thenAccept(
                        material -> {
                            ModelRenderable modelRenderable = ShapeFactory.makeCube(
                                    new Vector3(.01f, .01f, difference.length()),
                                    Vector3.zero(), material);

                            TextView tv = new TextView(appContext);
                            String text = String.valueOf(getRoundedDistance(cornerPair.getDifference().length()));
                            text += " m";
                            tv.setText(text);
                            tv.setTextColor(android.graphics.Color.WHITE);
                            tv.setBackgroundColor(android.graphics.Color.MAGENTA);
                            ViewRenderable.builder()
                                    .setView(appContext, tv)
                                    .build().thenAccept(viewRenderable -> {
                                LineNode node = new LineNode(cornerPair, modelRenderable, viewRenderable);
                                node.setParent(scene);
                                lineNodeMap.put(cornerPair, node);
                            });
                        });
    }

    public void updateModel() {
        if (floorTrackable == null || cornerMap.isEmpty()) {
            return;
        }
        updateCornersData();
        updateCornerNodes();
        updateLineNodes();
    }

    private void updateCornersData() {
        for (Map.Entry<TrackablePair, Corner> corner : cornerMap.entrySet()) {
            corner.getValue().update();
        }
    }

    private void updateCornerNodes() {
        for (Map.Entry<Corner, CornerNode> cornerNode : cornerNodeMap.entrySet()) {
            cornerNode.getValue().update();
        }
    }

    private void updateLineNodes() {
        Quaternion cameraDirection = scene.getCamera().getWorldRotation();
        for (Map.Entry<CornerPair, LineNode> lineNode : lineNodeMap.entrySet()) {
            lineNode.getValue().update(cameraDirection);
        }
    }

    public void saveModel() {
        ArrayList<float[]> corners = new ArrayList<>();
        // loading corners
        /*
        for (Map.Entry<TrackablePair, Corner> pair : cornerMap.entrySet()) {
            corners.add(pair.getValue().getPosition());
        }*/
        DebugData debugData = new DebugData();
        corners.addAll(debugData.getCornersArrays());
        Measurement.saveCorners(corners);
    }

    public void setFloorTrackable(Trackable trackable) {
        floorTrackable = trackable;
    }

    private float getRoundedDistance(float distance) {
        return Math.round(distance * 100.0f) / 100.0f;
    }
}
