package mateusz.arapplication.Calculations;

import com.google.ar.core.Trackable;

import java.util.ArrayList;

public class TrackablePair {
    Trackable t1;
    Trackable t2;

    public TrackablePair (Trackable t1, Trackable t2) {
        this.t1 = t1;
        this.t2 = t2;
    }

    public ArrayList<Trackable> getTrackables() {
        ArrayList<Trackable> trackableList = new ArrayList<>();
        trackableList.add(t1);
        trackableList.add(t2);
        return trackableList;
    }

    @Override
    public int hashCode() {
        return t1.hashCode() + t2.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof TrackablePair))
            return false;
        TrackablePair tp = (TrackablePair) (obj);
        return (tp.t1.equals(t1) && tp.t2.equals(t2));
    }
}
