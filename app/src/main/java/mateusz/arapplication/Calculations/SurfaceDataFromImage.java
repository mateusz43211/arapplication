package mateusz.arapplication.Calculations;

import com.google.ar.core.AugmentedImage;

public class SurfaceDataFromImage extends SurfaceData {
    private final AugmentedImage augmentedImage;

    public SurfaceDataFromImage(AugmentedImage augmentedImage) {
        this.augmentedImage = augmentedImage;
        updateData();
    }

    @Override
    public void updateData() {
        this.normal = augmentedImage.getCenterPose().getYAxis();
        this.translation = augmentedImage.getCenterPose().getTranslation();
    }
}
