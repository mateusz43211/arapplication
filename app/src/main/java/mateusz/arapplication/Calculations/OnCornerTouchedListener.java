package mateusz.arapplication.Calculations;

import mateusz.arapplication.AR.CornerNode;

public interface OnCornerTouchedListener {
    void checkTouchedCornerNode(CornerNode cornerNode);
}
