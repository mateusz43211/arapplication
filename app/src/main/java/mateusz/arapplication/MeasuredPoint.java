package mateusz.arapplication;

import com.google.ar.sceneform.math.Vector3;

public class MeasuredPoint {
    private Vector3 position;
    private PointType type;

    public enum PointType {
        CORNER,
        MARKER
    }

    public MeasuredPoint(float x, float y, float z) {
        position = new Vector3(x, y, z);
    }

    public void setAsCorner() {
        this.type = PointType.CORNER;
    }

    public void setAsMarker() {
        this.type = PointType.MARKER;
    }


}
