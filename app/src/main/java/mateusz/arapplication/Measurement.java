package mateusz.arapplication;

import java.util.ArrayList;

public class Measurement {

    private static ArrayList<MeasuredPoint> measuredPoints;

    public Measurement() {
        measuredPoints = new ArrayList<>();
    }

    public static void saveCorners(ArrayList<float[]> cornersCoordinates) {
        for (float[] corner : cornersCoordinates) {
            MeasuredPoint point = new MeasuredPoint(corner[0], corner[1], corner[2]);
            point.setAsCorner();
            measuredPoints.add(point);
        }
    }

    public static ArrayList<MeasuredPoint> getMeasuredPoints() {
        return measuredPoints;
    }


}
