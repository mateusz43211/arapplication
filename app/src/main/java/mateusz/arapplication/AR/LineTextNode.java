package mateusz.arapplication.AR;

import android.util.Log;
import android.widget.TextView;

import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ViewRenderable;

public class LineTextNode extends Node {
    private static final String TAG = "LineTextNode.class";
    private static final String UNIT_TEXT = " m";
    private TextView textView;
    private ViewRenderable renderable;
    private float lineLength;

    public LineTextNode(ViewRenderable renderable, float length) {
        super();
        this.renderable = renderable;
        this.lineLength = getRoundedDistance(length);

        setRenderable(renderable);
        renderable.setVerticalAlignment(ViewRenderable.VerticalAlignment.BOTTOM);
        textView = (TextView) renderable.getView();
    }

    private float getRoundedDistance(float distance) {
        return Math.round(distance * 100.0f) / 100.0f;
    }

    private void setTextFromLineLenght(float lineLength) {
        String textFromDistance = String.valueOf(lineLength);
        textFromDistance = textFromDistance + UNIT_TEXT;
        textView.setText(textFromDistance);
    }

    public void update(float currentLineLength) {
        float currentRoundedLength = getRoundedDistance(currentLineLength);
        if (lineLength != currentRoundedLength) {
            Log.d(TAG, "Different size of line");
            setTextFromLineLenght(currentRoundedLength);
        }
    }


}
