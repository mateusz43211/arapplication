package mateusz.arapplication.AR;

import android.util.Log;
import android.view.MotionEvent;

import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;

import mateusz.arapplication.Calculations.CornerPair;

public class LineNode extends Node {
    private final String TAG = "LineNode.class";
    private CornerPair cornerPair;
    private float modelLenght;
    private Vector3 scaleVector;
    private Vector3 lastDirection;
    private LineTextNode lineTextNode;
    private boolean rotationSet = false;

    public LineNode(CornerPair cornerPair, ModelRenderable model, ViewRenderable viewRenderable) {
        super();
        this.cornerPair = cornerPair;
        Log.d(TAG, "Constructor");
        setRenderable(model);
        setWorldPosition(cornerPair.getCenterPoint());

        final Vector3 difference = cornerPair.getDifference();
        final Vector3 directionFromTopToBottom = difference.normalized();
        final Quaternion rotationFromAToB = Quaternion.lookRotation(directionFromTopToBottom, Vector3.up());

        setWorldRotation(rotationFromAToB);
        modelLenght = cornerPair.getDifference().length();
        scaleVector = Vector3.zero();
        lastDirection = directionFromTopToBottom;

        lineTextNode = new LineTextNode(viewRenderable, modelLenght);
        lineTextNode.setParent(this);
        lineTextNode.setLocalPosition(new Vector3(0f, 0.25f, 0f));
    }

    public void update(Quaternion cameraRotation) {
        final Vector3 currentDiffence = cornerPair.getDifference();
        final float currentLength = currentDiffence.length();
        if (modelLenght != currentLength) {
            float scale = currentLength / modelLenght;
            rescaleModel(scale);
        }

        final Vector3 difference = cornerPair.getDifference();
        final Vector3 directionFromTopToBottom = difference.normalized();
        final Quaternion rotationFromAToB = Quaternion.lookRotation(directionFromTopToBottom, Vector3.up());
        setWorldRotation(rotationFromAToB);

        setWorldPosition(cornerPair.getCenterPoint());
        lastDirection.set(currentDiffence);

        lineTextNode.update(currentLength);
        lineTextNode.setWorldRotation(cameraRotation);
    }

    private void rescaleModel(float scale) {
        scaleVector.set(1f, 1f, scale);
        setWorldScale(scaleVector);
    }

    @Override
    public boolean onTouchEvent(HitTestResult hitTestResult, MotionEvent motionEvent) {
        setWorldScale(new Vector3(1f, 1f, 2f));
        return false;
    }
}
