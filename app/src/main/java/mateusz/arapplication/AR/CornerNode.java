package mateusz.arapplication.AR;

import android.util.Log;
import android.view.MotionEvent;

import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;

import mateusz.arapplication.Calculations.Corner;
import mateusz.arapplication.Calculations.OnCornerTouchedListener;

public class CornerNode extends Node {
    private final String TAG = "CornerNode.class";
    private Corner corner;
    private Vector3 position;
    private static final Color touchedColor = new Color(android.graphics.Color.GREEN);
    private static final Color untouchedColor = new Color(android.graphics.Color.RED);
    private OnCornerTouchedListener listener;

    public CornerNode(OnCornerTouchedListener listener, Corner corner, ModelRenderable modelRenderable) {
        super();
        this.listener = listener;
        this.corner = corner;
        setRenderable(modelRenderable);

        position = corner.getPosition();
        Log.d(TAG, "Corner after init: " + position.toString());
    }

    public void update() {
        Vector3 coordinates = corner.getPosition();
        position.set(coordinates);
        setWorldPosition(position);
        Log.d(TAG, "Position after update: " + position.toString());
    }

    @Override
    public boolean onTouchEvent(HitTestResult hitTestResult, MotionEvent motionEvent) {
        listener.checkTouchedCornerNode(this);
        return false;
    }

    public void setAsUntouched() {
        this.getRenderable().getMaterial()
                .setFloat3(MaterialFactory.MATERIAL_COLOR, untouchedColor);
    }

    public void setAsTouched() {
        this.getRenderable().getMaterial()
                .setFloat3(MaterialFactory.MATERIAL_COLOR, touchedColor);
    }

    public Corner getCorner() {
        return corner;
    }
}
