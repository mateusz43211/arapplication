package mateusz.arapplication.AR;

import android.content.Context;
import android.view.MotionEvent;

import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;

import mateusz.arapplication.Calculations.OnImageTouchedListener;

public class ImageNode extends Node {
    private AugmentedImage image;
    private static ImageNode lastNode = null;
    private ModelRenderable modelRenderable;
    private Vector3 position = new Vector3();
    private static final Color touchedColor = new Color(android.graphics.Color.YELLOW);
    private static final Color untouchedColor = new Color(android.graphics.Color.argb(30, 0, 0, 0));
    private Node helpNode;

    public ImageNode(AugmentedImage image, ModelRenderable modelRenderable) {
        this.image = image;
        this.modelRenderable = modelRenderable;

        helpNode = new Node();
        helpNode.setLocalPosition(Vector3.zero());
        helpNode.setRenderable(modelRenderable);
        setNodePosition(image.getCenterPose().getTranslation());
    }

    public void update() {
        setNodePosition(image.getCenterPose().getTranslation());
    }

    public void setAsUntouched() {
        this.removeChild(helpNode);
    }

    public void setAsTouched() {
        this.addChild(helpNode);
    }

    public AugmentedImage getImage() {
        return image;
    }

    private void setNodePosition(float[] coordinates) {
        position.set(coordinates[0], coordinates[1], coordinates[2]);
        this.setWorldPosition(position);
    }

}
