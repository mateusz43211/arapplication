package mateusz.arapplication.AR;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.ar.core.ArCoreApk;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.sceneform.math.Vector3;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class ARUtils {
    private static final String TAG = "ARUtils";
    private static final String IMAGE_DATABASE_NAME = "image_database.imgdb";

    private ARUtils() {
    }

    public static Session createArSession(Activity activity, boolean installRequested)
            throws UnavailableException {
        Session session = null;

        if (hasCameraPermission(activity)) {
            switch (ArCoreApk.getInstance().requestInstall(activity, !installRequested)) {
                case INSTALL_REQUESTED:
                    return null;
                case INSTALLED:
                    break;
            }

            session = new Session(activity);
            Config config = getSessionConfiguration(session, activity);
            session.configure(config);
        }
        return session;
    }

    private static Config getSessionConfiguration(Session session, Activity activity) {
        Config config = new Config(session);
        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
        config.setPlaneFindingMode(Config.PlaneFindingMode.HORIZONTAL_AND_VERTICAL);

        AugmentedImageDatabase augmentedImageDatabase = getAugmentedImageDatabase(session, activity);
        config.setAugmentedImageDatabase(augmentedImageDatabase);
        return config;
    }

    private static AugmentedImageDatabase getAugmentedImageDatabase(Session session, Activity activity) {
        AugmentedImageDatabase augmentedImageDatabase = null;
        try {
            InputStream inputStream = activity.getApplicationContext().getAssets().open(IMAGE_DATABASE_NAME);
            augmentedImageDatabase = AugmentedImageDatabase.deserialize(session, inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return augmentedImageDatabase;
    }

    public static boolean hasCameraPermission(Activity activity) {
        boolean hasPermission = false;
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            hasPermission = true;
        }
        return hasPermission;
    }

    public static void requestCameraPermission(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(
                activity, new String[]{Manifest.permission.CAMERA}, requestCode);
    }

    public static boolean shouldShowRequestPermissionRationale(Activity activity) {
        return ActivityCompat.shouldShowRequestPermissionRationale(
                activity, Manifest.permission.CAMERA);
    }

    public static void launchPermissionSettings(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", activity.getPackageName(), null));
        activity.startActivity(intent);
    }

    public static void logAugmentedImage(AugmentedImage image, String tag) {
        Log.d(tag, "Augmented Image Detected: " + image.getName());
        Log.d(tag, "Translation: " + Arrays.toString(image.getCenterPose().getTranslation()));

        float[] arrayX = image.getCenterPose().getTransformedAxis(0, 1f);
        float[] arrayY = image.getCenterPose().getTransformedAxis(1, 1f);
        float[] arrayZ = image.getCenterPose().getTransformedAxis(2, 1f);

        Log.d(tag, "X axis: " + Arrays.toString(arrayX));
        Log.d(tag, "Y axis: " + Arrays.toString(arrayY));
        Log.d(tag, "Z axis: " + Arrays.toString(arrayZ));
    }

    public static void logCorner(Vector3 corner, String tag) {
        Log.d(tag, "Corner: " + corner.toString());
    }


}
