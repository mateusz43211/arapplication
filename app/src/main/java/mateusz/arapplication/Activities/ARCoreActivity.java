package mateusz.arapplication.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.rendering.PlaneRenderer;
import com.google.ar.sceneform.rendering.Texture;

import mateusz.arapplication.AR.ARUtils;
import mateusz.arapplication.Calculations.ModelController;
import mateusz.arapplication.Calculations.OnCommunicateListener;
import mateusz.arapplication.R;

public class ARCoreActivity extends AppCompatActivity implements OnCommunicateListener {

    private static final int RC_PERMISSIONS = 0x123;
    private GestureDetector gestureDetector;
    private boolean installRequested;
    private ArSceneView arSceneView;
    private LinearLayout confirmLayout;
    private ModelController modelController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arcore);
        init();
    }

    private void init() {
        setupConfirmLayout();
        setupArSceneView();
        setupGestureDetector();
        setupPlaneRendererTexture();
        ARUtils.requestCameraPermission(this, RC_PERMISSIONS);

        modelController = new ModelController(getApplication(), arSceneView.getScene(), this);
    }

    void setupConfirmLayout() {
        confirmLayout = findViewById(R.id.confirmPlaneLayout);
        confirmLayout.setVisibility(View.GONE);

        Button yesButton = findViewById(R.id.yesButton);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelController.addCurrentFloorTrackable();
                hideConfirmLayout();
            }
        });

        Button noButton = findViewById(R.id.noButton);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideConfirmLayout();
            }
        });
    }

    private void showConfirmLayout() {
        if (confirmLayout.getVisibility() == View.GONE) {
            Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
            confirmLayout.startAnimation(slideUp);
            confirmLayout.setVisibility(View.VISIBLE);
        }
    }

    private void hideConfirmLayout() {
        if (confirmLayout.getVisibility() == View.VISIBLE) {
            confirmLayout.setVisibility(View.GONE);
        }
    }

    private void setupArSceneView() {
        arSceneView = findViewById(R.id.ar_scene_view);
        arSceneView.getScene().addOnUpdateListener(frameTime -> {
            Frame frame = arSceneView.getArFrame();
            modelController.updateArScene(frame);
        });

        arSceneView.getScene().setOnTouchListener((HitTestResult hitTestResult, MotionEvent event) -> {
            return gestureDetector.onTouchEvent(event);
        });
    }

    void setupGestureDetector() {
        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                onSingleTapEvent(e);
                return false;
            }
        });
    }

    void onSingleTapEvent(MotionEvent tap) {
        Frame frame = arSceneView.getArFrame();
        if (frame != null) {
            modelController.findTrackablesFromTap(frame, tap);
        }
    }

    void setupPlaneRendererTexture() {
        Texture.Sampler sampler = Texture.Sampler.builder()
                .setMinFilter(Texture.Sampler.MinFilter.LINEAR_MIPMAP_LINEAR)
//                .setMagFilter(Texture.Sampler.MagFilter.LINEAR)
                .setWrapMode(Texture.Sampler.WrapMode.REPEAT)
//                .setWrapModeR(Texture.Sampler.WrapMode.REPEAT)
//                .setWrapModeS(Texture.Sampler.WrapMode.REPEAT)
//                .setWrapModeT(Texture.Sampler.WrapMode.REPEAT)
                .build();

        Texture.builder().setSource(this, R.drawable.white_dot)
                .setSampler(sampler)
                .build().thenAccept(texture -> {
            arSceneView.getPlaneRenderer().getMaterial().thenAccept((material -> {
                material.setTexture(PlaneRenderer.MATERIAL_TEXTURE, texture);

            }));
        });
    }

    @Override
    public void showConfirmToFloorTrackable() {
        showConfirmLayout();
    }

    @Override
    public void showFloorIsNotFound() {
        Toast toast = Toast.makeText(this, "The floor is not found", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    //region Methods from Activity interface
    @Override
    protected void onResume() {
        super.onResume();

        if (arSceneView == null) {
            return;
        }

        if (arSceneView.getSession() == null) {
            try {
                Session session = ARUtils.createArSession(this, installRequested);
                if (session == null) {
                    installRequested = ARUtils.hasCameraPermission(this);
                    return;
                } else {
                    arSceneView.setupSession(session);
                }
            } catch (UnavailableException e) {
                //TODO exeption handle
            }
        }

        try {
            arSceneView.resume();
        } catch (CameraNotAvailableException ex) {
            //TODO message
            finish();
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (!ARUtils.hasCameraPermission(this)) {
            if (!ARUtils.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                ARUtils.launchPermissionSettings(this);
            } else {
                Toast.makeText(this,
                        "Camera permission is needed to run this application",
                        Toast.LENGTH_LONG)
                        .show();
            }
            finish();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (arSceneView != null) {
            arSceneView.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (arSceneView != null) {
            arSceneView.destroy();
        }

        modelController.saveModel();
        Intent intent = new Intent(ARCoreActivity.this, DisplayModelActivity.class);
        startActivity(intent);
        finish();
    }
    //endregion


}
