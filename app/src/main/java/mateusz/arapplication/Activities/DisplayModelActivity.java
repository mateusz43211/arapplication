package mateusz.arapplication.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.TextView;

import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.sceneform.Camera;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.SceneView;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.ArrayList;

import mateusz.arapplication.Calculations.DebugData;
import mateusz.arapplication.Calculations.Line;
import mateusz.arapplication.R;

public class DisplayModelActivity extends AppCompatActivity {

    private final float CORNER_RADIUS = 0.02f;
    private final String UNIT_TEXT = " m";
    private float scaleFactor = 1.1f;
    private SceneView sceneView;
    private Camera camera;
    private Color lineColor = new Color(android.graphics.Color.BLACK);
    private Color cornerColor = new Color(android.graphics.Color.RED);
    private Vector3 lookDirection;
    private ScaleGestureDetector scaleGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        setContentView(R.layout.activity_display_model);
        setupSceneView();
        setupCamera();
        loadModels();

        scaleGestureDetector = new ScaleGestureDetector(sceneView.getContext(), new ScaleListener());
    }

    private void setupSceneView() {
        sceneView = findViewById(R.id.Myscene_view);
        sceneView.getScene().setOnTouchListener(new Scene.OnTouchListener() {
            @Override
            public boolean onSceneTouch(HitTestResult hitTestResult, MotionEvent motionEvent) {
//                scaleGestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
    }

    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();

            scaleFactor = Math.max(0.8f, Math.min(scaleFactor, 1.2f));

            Vector3 lastPos = camera.getWorldPosition();
            Vector3 offset = lookDirection.scaled(scaleFactor);

            camera.setWorldPosition(Vector3.add(lastPos, offset));
            Log.d("SCALEDETECTOR", camera.getWorldPosition().toString());

            return true;
        }
    }


    private void setupCamera() {
        camera = sceneView.getScene().getCamera();
        camera.setLocalPosition(new Vector3(0f, 2f, 0f));

        lookDirection = new Vector3(-0.0126f, -0.38396f, -0.0134f).normalized();
        camera.setLookDirection(lookDirection);
    }

    private void loadModels() {
        DebugData debugData = new DebugData();
        ArrayList<Vector3> corners = debugData.getCornersVectors();

        for (Vector3 pos : corners) {
            createCornerModel(pos);
        }

        createLineModels(corners);
    }

    private void createLineModels(ArrayList<Vector3> corners) {
        Line line = new Line(corners.get(0), corners.get(1));
        createLineModel(line);

        line = new Line(corners.get(1), corners.get(2));
        createLineModel(line);

        line = new Line(corners.get(2), corners.get(3));
        createLineModel(line);

        line = new Line(corners.get(3), corners.get(0));
        createLineModel(line);
    }

    private void createCornerModel(Vector3 position) {
        MaterialFactory.makeOpaqueWithColor(getApplicationContext(), cornerColor)
                .thenAccept(material -> {
                    ModelRenderable model = ShapeFactory
                            .makeSphere(CORNER_RADIUS, Vector3.zero(), material);
                    Node node = new Node();
                    node.setParent(sceneView.getScene());
                    node.setLocalPosition(position);
                    node.setRenderable(model);
                });
    }

    private void createLineModel(Line line) {

        final Vector3 difference = line.getDifference();
        final Vector3 directionFromTopToBottom = difference.normalized();
        final Quaternion rotationFromAToB = Quaternion.lookRotation(directionFromTopToBottom, Vector3.up());

        MaterialFactory.makeOpaqueWithColor(getApplicationContext(), lineColor)
                .thenAccept(
                        material -> {
                            ModelRenderable model = ShapeFactory.makeCube(
                                    new Vector3(.01f, .01f, difference.length()),
                                    Vector3.zero(), material);
                            Node node = new Node();
                            node.setParent(sceneView.getScene());
                            node.setRenderable(model);
                            node.setWorldPosition(line.getCenterPoint());
                            node.setWorldRotation(rotationFromAToB);
                        });

        createTextForLine(line);
    }

    private void createTextForLine(Line line) {
        float distance = getRoundedDistance(line.getDifference().length());
        String textFromDistance = String.valueOf(distance);

        textFromDistance = textFromDistance + UNIT_TEXT;

        TextView tv = new TextView(getApplicationContext());
        tv.setText(textFromDistance);

        ViewRenderable.builder()
                .setView(getApplicationContext(), tv)
                .build()
                .thenAccept(renderable -> {
                    createTextModel(renderable, line);
                });

    }

    private float getRoundedDistance(float distance) {
        return Math.round(distance * 100.0f) / 100.0f;
    }

    private void createTextModel(ViewRenderable renderable, Line line) {
        Quaternion textRotation = getWorldRotationForText(line);
        Vector3 textPosition = getWorldPositionForText(line);

        Node node = new Node();
        node.setParent(sceneView.getScene());
        node.setRenderable(renderable);
        node.setWorldPosition(textPosition);
        node.setWorldRotation(textRotation);

    }

    private Vector3 getWorldPositionForText(Line line) {
        final Vector3 difference = line.getDifference();
        final Vector3 directionOfLine = difference.normalized();
        final Vector3 cross = Vector3.cross(directionOfLine, lookDirection).normalized();

        final Vector3 lineCenter = line.getCenterPoint();
        Vector3 toAdd = getScaledVectorOffset(cross);
        Vector3 result = Vector3.add(lineCenter, toAdd);
        return result;
    }

    private Vector3 getScaledVectorOffset(Vector3 cross) {
        final float offset = 0.02f;
        final float scale = offset / cross.length();
        return cross.scaled(scale);
    }

    private Quaternion getWorldRotationForText(Line line) {
        final Vector3 difference = line.getDifference();
        final Vector3 directionOfLine = difference.normalized();
        final Vector3 normalToLine = Vector3.cross(directionOfLine, lookDirection);

        Quaternion rotationFromAToB = Quaternion.rotationBetweenVectors(Vector3.up(), normalToLine);
        Quaternion rotationFromBToC = Quaternion.lookRotation(normalToLine, Vector3.up());
        final Quaternion result = Quaternion.multiply(rotationFromAToB, rotationFromBToC);

        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            sceneView.resume();
        } catch (CameraNotAvailableException e) {
            throw new AssertionError("Failed to resume SceneView", e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        sceneView.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sceneView.destroy();
    }
}
