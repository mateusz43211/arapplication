package mateusz.arapplication.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mateusz.arapplication.Measurement;
import mateusz.arapplication.R;

public class MainActivity extends AppCompatActivity {

    private Button measureButton;
    private Button displayButton;
    private Measurement measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        setupMeasureButton();
        setupDisplayButton();
        measurement = new Measurement();
    }

    private void setupMeasureButton() {
        measureButton = findViewById(R.id.measureButton);
        measureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ARCoreActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupDisplayButton() {
        displayButton = findViewById(R.id.display3dButton);
        displayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DisplayModelActivity.class);
                startActivity(intent);
            }
        });
    }
}
